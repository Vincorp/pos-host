package com.vretail.poshost.bean.rabbit;

import com.vretail.poshost.config.VEnv;
import com.vretail.poshost.service.TransactionReceiverService;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created on 1/26/17.
 *
 * @author felixsoewito
 */
@Configuration
public class RabbitBean {

    private static final String TOPIC_NAME = VEnv.RABBIT_TOPIC.value;
    private static final String QUEUE_NAME = VEnv.RABBIT_QUEUE.value;

    @Bean
    public Queue transactionQueue() {
        return new Queue(QUEUE_NAME, true, false, false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(TOPIC_NAME);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME);
    }

    @Bean
    ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(VEnv.RABBIT_HOST.value);
        connectionFactory.setUsername(VEnv.RABBIT_USER.value);
        connectionFactory.setPassword(VEnv.RABBIT_PASSWORD.value);
        connectionFactory.setVirtualHost(VEnv.RABBIT_VHOST.value);
        return connectionFactory;
    }

    @Bean
    public SimpleMessageListenerContainer listenerContainer(ConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(QUEUE_NAME);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter() {
        return new MessageListenerAdapter(new TransactionReceiverService(), "receiveTransaction");
    }

    /**
     * @return the admin bean that can declare queues etc.
     */
    @Bean
    public AmqpAdmin amqpAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

}
