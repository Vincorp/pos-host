package com.vretail.poshost.service;

import com.vretail.poscommon.model.Person;
import com.vretail.poscommon.util.VLogger;
import com.vretail.poshost.config.VEnv;

/**
 * Created on 1/26/17.
 *
 * @author felixsoewito
 */
public class TransactionReceiverService {

    public void receiveTransaction(Person person) {
        VLogger.info(VEnv.V_DOMAIN.value, new Object(), "received [x]" + person.getName());
    }
}
