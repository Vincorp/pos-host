package com.vretail.poshost.controller;

import com.vretail.poscommon.util.TimestampUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 1/25/17.
 *
 * @author felixsoewito
 */
@RestController
public class VController {

    @RequestMapping(
            method = RequestMethod.GET,
            path = {"home", "/", "index"})
    public String hello() {
        return "Ready to go, sir";
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "time")
    public String getLocalTime(){
        return TimestampUtil.generateIso8601();
    }
}
