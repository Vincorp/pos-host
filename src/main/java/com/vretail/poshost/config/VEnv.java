package com.vretail.poshost.config;

import com.vretail.poscommon.util.VLogger;

/**
 * Created on 1/25/17.
 *
 * @author felixsoewito
 */
public enum VEnv {

    V_DOMAIN("V_DOMAIN", "POS-HOST"),
    RABBIT_QUEUE("RABBIT_QUEUE", "vretail-transaction"),
    RABBIT_TOPIC("RABBIT_TOPIC", "vretail-transaction"),
    RABBIT_USER("RABBIT_USER", "vretail"),
    RABBIT_PASSWORD("RABBIT_PASSWORD", "vretail"),
    RABBIT_VHOST("RABBIT_VHOST", "vretail"),
    RABBIT_HOST("RABBIT_HOST", "192.168.43.18");

    public String key;
    public String value;

    VEnv(String key, String defaultValue) {
        this.key = key;
        String envVal = System.getenv(key);

        if (envVal != null) {
            VLogger.info("SETUP", new Object(),"Key(" + key + ")  => " + envVal);
            this.value = envVal;
        } else {
            VLogger.warn("SETUP", new Object(),"!!! Key(" + key + ") => " + defaultValue + " (Default)");
            this.value = defaultValue;
        }
    }
}
