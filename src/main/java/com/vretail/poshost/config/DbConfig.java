package com.vretail.poshost.config;

import com.vretail.poscommon.util.RsaUtil;
import com.vretail.poscommon.util.VLogger;

/**
 * Created on 1/31/17.
 *
 * @author felixsoewito
 */
public enum DbConfig {

    DB_USERNAME("DB_USERNAME", "default"),
    DB_PASSWORD("DB_PASSWORD", "default"),
    DB_URL("DB_URL", "localhost");

    public String key;
    public String value;

    DbConfig(String key, String defaultValue) {
        this.key = key;
        String envVal = System.getenv(key);

        if (envVal != null) {
            this.value  = RsaUtil.decrypt(envVal);
        } else {
            this.value = defaultValue;
        }
    }


}
