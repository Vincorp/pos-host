package com.vretail.poshost;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.vretail.poscommon.util.VLogger;
import com.vretail.poshost.config.VEnv;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Created on 1/25/17.
 *
 * @author felixsoewito
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Application.class, args);

        String[] beans = context.getBeanDefinitionNames();
        for (String bean : beans) {
            VLogger.info(VEnv.V_DOMAIN.value, beans, bean);
        }
    }

    @Bean
    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializationInclusion(JsonInclude.Include.NON_NULL);
        builder.failOnUnknownProperties(false);
        return builder;
    }
}
